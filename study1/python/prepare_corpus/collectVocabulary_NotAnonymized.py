import os


from collections import defaultdict
for corpus in ["cnn", "dailymail"]:
   vocabulary = defaultdict(int)
   for partition in ["test", "validation", "training"]:
     print(corpus, partition)
     with open(f'/john5/scr1/mhahn/CHO/{corpus}/questions/{corpus}_{partition}_nonAnonymized.txt', "r") as inFile:
        for line in inFile:
           for word in line.split(" "):
               if len(word) == 0:
                   continue
               vocabulary[word.strip()] += 1
   vocabulary_list = list(vocabulary.items())
   vocabulary_list = sorted(vocabulary_list, key=lambda x:x[1], reverse=True)
   with open(f"vocabularies/{corpus}_nonAnonymized.txt", "w") as outFile:
       for i in range(min(500000, len(vocabulary_list))):
           print(f"{str(i)}\t{vocabulary_list[i][0]}\t{str(vocabulary_list[i][1])}", file=outFile)
