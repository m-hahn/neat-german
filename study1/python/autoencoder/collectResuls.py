import glob
files = glob.glob("SCRATCH/results/*Eval*")
results = []
for f in files:
    with open(f, "r") as inFile:
        args, heldout = inFile.read().strip().split("\n")
        heldout = float(heldout.strip("[").strip("]"))
        results.append((heldout, args))
results = sorted(results, reverse=True)
with open(f"logs/{__file__}.txt", "w") as outFile:
  for x,y  in results:
    print(x,y, file=outFile)
