import glob
import sys
import subprocess
__file__ = __file__.split("/")[-1]

print(f"SCRATCH/checkpoints/{__file__.replace('_Eval_RUNALL.py', '.py')}_*.ckpt")
checkpoints = glob.glob(f"SCRATCH/checkpoints/{__file__.replace('_Eval_RUNALL.py', '.py')}_*.ckpt")
print(checkpoints)
import random
random.shuffle(checkpoints)
for f in checkpoints:
   print(f)
   ID = f.replace(".ckpt", "").split("_")[-1]
   print(["python3", __file__.replace('_RUNALL.py', '.py'), "--myID="+ID, "--corpus=wiki_general"])
   subprocess.call(["python3", __file__.replace('_RUNALL.py', '.py'), "--myID="+ID, "--corpus=wiki_general"], stderr=sys.stdout, stdout=sys.stdout)
