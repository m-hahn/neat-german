files = []
files.append("SCRATCH/wiki_general_train.txt")
files.append("SCRATCH/wiki_spektrum_bio_train.txt")
files.append("SCRATCH/wiki_spektrum_phys_train.txt")

counter = {}

def printVocab():
    words = sorted(counter.items(), key=lambda x:x[1], reverse=True)
    with open("german.txt", "w") as outFile:
        q = 0
        for w, c in words:
            print(f"{q}\t{w}\t{c}", file=outFile)
            q += 1


i = 0
for f in files:
    with open(f, "r") as inFile:
        for line in inFile:
            i += 1
            if i % 1e3 == 0:
                print(i)
            if i % 1e4 == 0:
                printVocab()
            for w in line.strip().split(" "):
                counter[w] = counter.get(w,0) + 1


printVocab()


