# train_attention_6.py: THIS one works nice and fast

import random
import argparse

parser = argparse.ArgumentParser()


parser.add_argument('--corpus', type=str, default="cnn")
parser.add_argument('--batchSize', type=int, default=64) #random.choice([16, 32]))
parser.add_argument('--learning_rate', type=float, default=random.choice([1.0]))
parser.add_argument('--glove', type=bool, default=True)
parser.add_argument('--dropout', type=float, default=random.choice([0.0, 0.05, 0.1, 0.15, 0.2]))
parser.add_argument('--myID', type=int, default=random.randint(1000,100000000))
parser.add_argument('--SEQUENCE_LENGTH', type=int, default=50)
parser.add_argument('--LOAD_CKPT', type=str, default="TODO") #random.choice([1.5, 1.75, 2, 2.25, 2.5]))

args = parser.parse_args()

SEQUENCE_LENGTH = args.SEQUENCE_LENGTH

vocabulary = [x.split("\t") for x in open(f"vocabularies/{args.corpus}_nonAnonymized.txt", "r").read().strip().split("\n")]
itos = [x[1] for x in vocabulary]
stoi = dict([(x[1], int(x[0])) for x in vocabulary])

def unique(l):
    s = set()
    r = []
    for x in l:
      if x not in s:
        s.add(x)
        r.append(x)
    return r

answerDistribution = [0, [0 for _ in range(600)]]
 

import gzip

def loadQACorpus(corpus, partition, batchSize, permuteEntities=True):
   assert partition in ["test", "training", "validation"]
   with gzip.open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/DEEPMIND_CORPUS_CHO/cnn_{partition}_nonAnonymized.txt.gz", "rb") as inFile1:
    with gzip.open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/DEEPMIND_CORPUS_CHO/dailymail_{partition}_nonAnonymized.txt.gz", "rb") as inFile2:
     while True:
        buff = []
        print("Filling buffer...")
        for _ in range(10*args.batchSize):
         try:
           buff.append(next(inFile1).decode().strip().split(" "))
           buff.append(next(inFile2).decode().strip().split(" "))
         except StopIteration:
            break
        if len(buff) == 0:
           break
        random.shuffle(buff)
        concatenated = []
        for x in buff:
           for y in x:
            concatenated.append(y)
        partitions = []
        for i in range(int(len(concatenated)/SEQUENCE_LENGTH)+1):
          r = concatenated[i*SEQUENCE_LENGTH:(i+1)*SEQUENCE_LENGTH]
          if len(r) > 0:
            partitions.append(r)
        random.shuffle(partitions)
        for i in range(int(len(partitions)/args.batchSize)+1):
          r = partitions[i*args.batchSize:(i+1)*args.batchSize]
          if len(r) > 0:
            yield r
OOV = 2
SKIPPED = 1
PAD = 0
PLACEHOLDER = 3

#training = 


def numerify(token):
   if token == "@placeholder":
      return PLACEHOLDER
   elif token not in stoi or stoi[token] >= 50000:
      return OOV
   else:
      return stoi[token]+4

import torch





word_embeddings = torch.nn.Embedding(num_embeddings = 50000+4, embedding_dim = 200).cuda()


if args.glove:
 with open("/u/scr/mhahn/glove/glove.6B.200d.txt", "r") as inFile:
  print("Loading embeddings")
  counter = 0
  for line in inFile:
    counter += 1
    line = next(inFile).strip().split(" ")
    word = line[0]
    if word in stoi and stoi[word] < 50000:
       #print(stoi[word])
       embedding = torch.FloatTensor([float(x) for x in line[1:]])
       word_embeddings.weight.data[stoi[word]+4] = embedding
       # print(counter, word)
    if counter > 100000:
      break
  print("Done loading embeddings")

########################
reader = torch.nn.LSTM(200, 1024, 1).cuda()
#reconstructor = torch.nn.LSTM(200, 1024, 1).cuda()
output = torch.nn.Linear(1024, 50000 + 4).cuda()

input_dropout = torch.nn.Dropout(args.dropout)

nllLoss = torch.nn.NLLLoss(reduction="none", ignore_index=PAD)
crossEntropy = torch.nn.CrossEntropyLoss(reduction="none", ignore_index=PAD)

components_lm = [word_embeddings, reader, output]




def parameters():
 for c in components_lm:
   for param in c.parameters():
      yield param

optimizer = torch.optim.SGD(parameters(), lr = args.learning_rate)

state = torch.load(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2020/train_lm.py_{args.LOAD_CKPT}.ckpt")

print("args", state["args"])
for i in range(len(components_lm)):
   components_lm[i].load_state_dict(state["components"][i])



def forwardAttention(batch):
    texts = [[numerify(y) for y in x["text"][:TEXT_LENGTH_BOUND]] for x in batch] # [:500]
    questions = [set([numerify(y) for y in x["question"]]) for x in batch]
    text_length = max([len(x) for x in texts])
    position = torch.FloatTensor([list(range(text_length)) for _ in batch]).cuda().t()
    condition = torch.FloatTensor([[0 if i < len(batch)/2 else 1 for _ in range(text_length)] for i in range(len(batch))]).cuda().t()
    condition_ = condition - 0.5
    position = (position)/500 - 0.5
    positionTimesCondition = condition_ * position
    occursInQuestion = torch.FloatTensor([[1 if j < len(texts[i]) and texts[i][j] in questions[i] else 0 for j in range(text_length)] for i in range(len(batch))]).cuda().t() - 0.1
    occursInQuestion = occursInQuestion * condition
    occursInQuestionAverage = torch.FloatTensor([0 for i in range(len(batch))]).cuda()
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)

    texts = torch.LongTensor(texts).cuda().transpose(0,1)
    texts_num = texts
    texts = word_embeddings(texts).detach()
    global featuresAverage
    featuresAverage_ = featuresAverage.clone()
    attentionLogprobabilities = []
    attentionDecisionsList = []
    zeroProbabilities = torch.zeros(len(batch)).cuda()
    runningAverage = torch.nn.functional.sigmoid(runningAverageParameter)
    oneMinus = (1-runningAverage)
    runningAverage_ = torch.cat([ oneMinus*torch.pow(runningAverage, 10-i) for i in range(10)] + [runningAverage], dim=0).unsqueeze(1)
    attentionScores = []
    ones = torch.zeros(text_length, len(batch)).cuda()+1
#    print(featuresAverage_)



 
  
    embedding = texts
    #print(embedding)

#    attentionLogit = linear(features) + bilinear(features, embedding)
    if random.random() < 0.01:
        print(bilinear.weight.size(), embedding.size())
        perWordWeights = torch.matmul(bilinear.weight.squeeze(2), embedding.transpose(1,2))[:10].cpu().detach().numpy()
        print("Linear1", linear.weight.data)
        print("Position\tCondition\tPositionXCondition\tInQuestion")
        for i in range(min(10, len(batch[0]["text"]))):
           print(i, "\t", batch[0]["text"][i], "\t", "\t".join([str(float(x)) for x in perWordWeights[i,:,0]]))

    #print(linear.weight.abs().max())
    #print(bilinear.weight.abs().max())
    #print("attentionLogit", attentionLogit)
    attentionLogProbability = torch.nn.functional.logsigmoid(torch.where(attentionDecisions == 1, attentionLogit, -attentionLogit))
    #print(attentionProbability)
    #print(attentionDecisions) 
#    quit()
    return attentionDecisions, attentionLogProbability, attentionProbability




def forward(batch, calculateAccuracy=False):
    texts = [[PAD] + [numerify(y) for y in x] + [PAD] for x in batch] # [:500]
    text_length = max([len(x) for x in texts])
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)
    texts =  torch.LongTensor(texts).cuda()

    mask = torch.FloatTensor([1 for _ in range(len(batch))]).cuda()
    masked = torch.LongTensor([SKIPPED]).cuda().unsqueeze(1).expand(len(batch), texts.size()[1]-1)
#    print(masked.size())
    hidden = None
    outputs = []


    mask = torch.bernoulli(torch.FloatTensor([[0.64 for _ in range(texts.size()[0])] for _ in range(texts.size()[1]-1)]).cuda()).transpose(0,1)

#    print(mask.size(), texts.size(), masked.size())
    embedded_ = word_embeddings(torch.where(mask==1.0, texts[:,:-1], masked)).transpose(0,1)
    outputs_reader, hidden = reader(embedded_)

#    embedded = word_embeddings(texts).transpose(0,1)
 #   if not calculateAccuracy:
  #     embedded = input_dropout(embedded)
#    hidden = (hidden[0].unsqueeze(0), hidden[1].unsqueeze(0))
#    outputs_decoder, _ = reconstructor(embedded[:-1], hidden)
    targets = texts.transpose(0,1).contiguous()
    targets = targets[1:]
    outputs_cat = output(outputs_reader) # dim=0))
    loss = crossEntropy(outputs_cat.view(-1, 50004), targets.view(-1)).view(outputs_cat.size()[0], outputs_cat.size()[1])



    if random.random() < 0.02:
       sequenceLengthHere= text_length-2
#       assert sequenceLengthHere == SEQUENCE_LENGTH, (text_length, SEQUENCE_LENGTH)
       loss_reader = loss[:10, 0].cpu()

       print("\t".join(["Pos", "Word", "Pred", "Rec", "AttProb", "Att?"]))
       for j in range(10):
         try:
           print("\t".join([str(y) for  y in [j, batch[0][j]] +[round(float(x),4) for x in [loss_reader[j]]]]))
         except IndexError:
           print("IndexError", j)
           pass
#       quit()

    #print(attentionLogProbability.size())
    loss = loss.mean(dim=0)
    return loss

args.clip_type = random.choice([2, "inf", "None"])
args.clip_bound = random.choice([2, 5, 10, 15])

fixationRunningAverageByCondition = [0.5,0.5]
lossAverageByCondition = [10.0, 10.0]

def backward(loss, printHere=True):
   if random.random() < 0.99:
     loss1 = float(loss.mean())
     lossAverageByCondition[0] = 0.99 * lossAverageByCondition[0] + (1-0.99) * loss1
     if printHere:
       print("LossByCondition", lossAverageByCondition)
   optimizer.zero_grad()
   loss_ = loss.mean()
   loss_.backward()
   if args.clip_type is not "None":
#      print("NROM MAX", max(p.grad.detach().abs().max().cpu() for p in parameters()))
      torch.nn.utils.clip_grad_norm_(parameters(), args.clip_bound, norm_type=args.clip_type)
   optimizer.step()

import time


learning_rate=args.learning_rate

devLosses = []
lossRunningAverage = 6.4
noImprovement = 0
for epoch in range(10000):
  
  if epoch > 0:
    validLoss = []
    examplesNumber = 0
    for batch in  loadQACorpus(args.corpus, "validation", args.batchSize, permuteEntities=True):
     with torch.no_grad():
       loss = forward(batch, calculateAccuracy = True)
       loss = float(loss.mean())
       print("VALID", loss, examplesNumber)


     validLoss.append(float(loss)*len(batch))
     examplesNumber += len(batch)
    devLosses.append(sum(validLoss)/examplesNumber)
    print(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/study1Scores_2020/accuracy_{__file__}_{args.myID}.txt")
    with open(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/study1Scores_2020/accuracy_{__file__}_{args.myID}.txt", "w") as outFile:
       print(args, file=outFile)
       print(devLosses, file=outFile)
    if len(devLosses) >1 and devLosses[-1] > devLosses[-2]:
       learning_rate *= 0.8
       optimizer = torch.optim.SGD(parameters(), lr = learning_rate)
       noImprovement += 1
    elif len(devLosses) > 1 and devLosses[-1] < max(devLosses):
       torch.save({"devLosses" : devLosses, "args" : args, "components" : [x.state_dict() for x in components_lm], "learning_rate" : learning_rate}, f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2020/{__file__}_{args.myID}.ckpt")
       noImprovement = 0
    if noImprovement > 5:
      print("End training, no improvement for 5 epochs")
      break
  words = 0
  timeStart = time.time()
  counter = 0
  for batch in loadQACorpus(args.corpus, "training", args.batchSize, permuteEntities=True):
    counter += 1
    printHere = (counter % 25) == 0
    loss = forward(batch)
    backward(loss, printHere=printHere)
    loss = float(loss.mean())
    lossRunningAverage = 0.99 *lossRunningAverage + (1-0.99) * float(loss)
    words += len(batch)
    if counter % 20000 == 0 and SEQUENCE_LENGTH < args.SEQUENCE_LENGTH:
       SEQUENCE_LENGTH = min(SEQUENCE_LENGTH+5, args.SEQUENCE_LENGTH)
    if counter % 200000 == 0 and SEQUENCE_LENGTH == args.SEQUENCE_LENGTH and epoch == 0:
       torch.save({"devLosses" : devLosses, "args" : args, "components" : [x.state_dict() for x in components_lm], "learning_rate" : learning_rate}, f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2020/{__file__}_{args.myID}.ckpt")
     
    if printHere:
       print(devLosses)
       print(float(loss), lossRunningAverage, args, __file__, counter)
       print(SEQUENCE_LENGTH*words/(time.time()-timeStart), "words per second", SEQUENCE_LENGTH)
      

