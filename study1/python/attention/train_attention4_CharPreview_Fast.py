# train_attention_6.py: THIS one works nice and fast

__file__ = __file__.split("/")[-1]

import random
import argparse

parser = argparse.ArgumentParser()


parser.add_argument('--corpus', type=str, default="cnn")
parser.add_argument('--batchSize', type=int, default=random.choice([128, 256, 512, 1024]))
parser.add_argument('--learning_rate', type=float, default=random.choice([0.1, 0.1, 0.2, 0.5]))
parser.add_argument('--dropout', type=float, default=0.0)
parser.add_argument('--myID', type=int, default=random.randint(1000,100000000))
parser.add_argument('--SEQUENCE_LENGTH', type=int, default=50)
parser.add_argument('--LAMBDA', type=float, default=random.choice([0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75, 3]))
parser.add_argument('--REWARD_FACTOR', type=float, default=0.1)
parser.add_argument('--ENTROPY_WEIGHT', type=float, default=0.005) #random.choice([0.0001, 0.001, 0.01, 0.1]))
parser.add_argument('--previewLength', type=int, default=random.choice([4]))
parser.add_argument('--lengthUntil', type=int, default=random.choice([0]))
parser.add_argument('--load_from', type=int, default=random.choice([23878455,5439711]))

args = parser.parse_args()

args.clip_type = random.choice([2, "inf"]) # 2
args.clip_bound = random.choice([2, 5, 10, 15])


SEQUENCE_LENGTH = args.SEQUENCE_LENGTH
assert SEQUENCE_LENGTH==50

vocabulary = [x.split("\t") for x in open(f"vocabularies/german.txt", "r").read().strip().split("\n")]
itos = [x[1] for x in vocabulary]
stoi = dict([(x[1], int(x[0])) for x in vocabulary])

itos_characters = set()
for x in itos:
   for y in x:
      itos_characters.add(y)
      itos_characters.add(y.lower())
      itos_characters.add(y.upper())
itos_characters = ["OOV", "PAD", "EOS", "SOS"] + (sorted(list(itos_characters))[:100])
stoi_characters = dict(list(zip(itos_characters, range(len(itos_characters)))))
print(stoi_characters)


def unique(l):
    s = set()
    r = []
    for x in l:
      if x not in s:
        s.add(x)
        r.append(x)
    return r

answerDistribution = [0, [0 for _ in range(600)]]
 

import gzip

assert args.corpus in ["wiki_general", "wiki_spektrum_bio", "wiki_spektrum_phys"]

def loadQACorpus(corpus, partition, batchSize, permuteEntities=True):
   assert partition in ["test", "train"]
   with open(f"SCRATCH/{corpus}_{partition}.txt", "r") as inFile1:
     while True:
        buff = []
        print("Filling buffer...")
        for _ in range(10*args.batchSize):
         try:
           buff.append(next(inFile1).strip().split(" "))
         except StopIteration:
            break
        if len(buff) == 0:
           break
        random.shuffle(buff)
        concatenated = []
        for x in buff:
           for y in x:
            concatenated.append(y)
        partitions = []
        for i in range(int(len(concatenated)/SEQUENCE_LENGTH)+1):
          r = concatenated[i*SEQUENCE_LENGTH:(i+1)*SEQUENCE_LENGTH]
          if len(r) > 0:
            partitions.append(r)
        random.shuffle(partitions)
        for i in range(int(len(partitions)/args.batchSize)+1):
          r = partitions[i*args.batchSize:(i+1)*args.batchSize]
          if len(r) > 0:
            yield r
OOV = 2
SKIPPED = 1
PAD = 0
PLACEHOLDER = 3

#training = 


def lengthBin(word):
  if len(word) < args.lengthUntil:
     return len(word)
  return args.lengthUntil


def makePrefix(x):
   preview = x[:args.previewLength]+"_"+str(lengthBin(x))
   return preview


def numerify(token):
   if token == "@placeholder":
      return PLACEHOLDER
   elif token not in stoi or stoi[token] >= 50000:
      return OOV
   else:
      return stoi[token]+4

def numerify_preview_characters(token):
#   token = token.replace("—", "-").replace('”', '"').replace('“', '"').replace("»", '"').replace("…", "...").replace("‘", "'").replace("’", "'").replace("!", "!").replace("–", "-") # this relates to DailyMail vs CNN
   token = token[:args.previewLength]
   #for x in token:
  #   if x not in stoi_characters:
 #       print("UNKNOWN", x)
   return [stoi_characters.get(x, stoi_characters["OOV"]) for x in token] + [stoi_characters["PAD"] for _ in range(args.previewLength-len(token))]




def numerify_preview(token):
#   token = token.lower()
#   token = token.replace("—", "-").replace('”', '"').replace('“', '"').replace("»", '"').replace("…", "...").replace("‘", "'").replace("’", "'").replace("!", "!").replace("–", "-") # this relates to DailyMail vs CNN
   preview = makePrefix(token)
   if preview not in stoi_prefixes:
      return OOV
   else:
      return stoi_prefixes[preview]


import torch





word_embeddings = torch.nn.Embedding(num_embeddings = 50000+4, embedding_dim = 200).cuda()
char_embeddings = torch.nn.Embedding(num_embeddings = 100+4, embedding_dim = 128).cuda()

########################
reader = torch.nn.LSTM(200, 1024, 1).cuda()
reconstructor = torch.nn.LSTM(200, 1024, 1).cuda()
output = torch.nn.Linear(1024, 50000 + 4).cuda()

input_dropout = torch.nn.Dropout(args.dropout)

nllLoss = torch.nn.NLLLoss(reduction="none", ignore_index=PAD)
crossEntropy = torch.nn.CrossEntropyLoss(reduction="none", ignore_index=PAD)

components_lm = [word_embeddings, reader, reconstructor, output]

ONLY_REC = True
WITH_CONTEXT = False
WITH_LM = False

if not ONLY_REC:
  loaded = torch.load(f"SCRATCH/checkpoints/train_autoencoder.py_{args.load_from}.ckpt")
else:
  loaded = torch.load(f"SCRATCH/checkpoints/train_autoencoder_onlyRec.py_{args.load_from}.ckpt")
#{"devLosses" : devLosses, "args" : args, "components" : [x.state_dict() for x in components_lm], "learning_rate" : learning_rate}, )
for i in range(len(loaded["components"])):
   components_lm[i].load_state_dict(loaded["components"][i])


if WITH_CONTEXT:
  # Use a bilinear module to combine word embedding with context information
  bilinear = torch.nn.Bilinear(1024, 328, 1).cuda()
else:
  # Use a linear module for derive attention logit from word embedding
  bilinear = torch.nn.Linear(328, 1).cuda()
bilinear.weight.data.zero_()
bilinear.bias.data.zero_()




def findFirstSatisfying(startIncl, endIncl, condF):
   if condF(startIncl):
     return startIncl
   i = endIncl
   while True:
      if startIncl == i:
         return i
      if startIncl+1 == i:
         return i
      med = int((startIncl + i)/2)
      if condF(med):
          i = med
      else:
         startIncl = med
   

associated = {}

vocab_ = sorted(itos[:50000], key=lambda x:(makePrefix(x), len(x)))
print(vocab_[:100])
lastPrefix = ""
startPos = 0
notDone = set(range(50005))
#word_embeddings_preview.weight.data = word_embeddings.weight.data.clone().mean(dim=0).unsqueeze(0).expand(word_embeddings.weight.data.size()[0], -1)

#word_embeddings_preview.weight.data.zero_()


itos_prefixes = ["PAD", "SKIPPED", "OOV", "PLACEHOLDER"]
stoi_prefixes = {x : itos_prefixes.index(x) for x in itos_prefixes}
prefix_embeddings_matrix = [word_embeddings.weight.data[i].clone() for i in range(len(itos_prefixes))]

startPos = 0
lastPrefix = makePrefix(vocab_[0])
for i in range(1,len(vocab_)):
   prefix = makePrefix(vocab_[i])
   if prefix in stoi_prefixes:
     continue
   elif prefix != lastPrefix:
       first = startPos #findFirstSatisfying(startPos, j, lambda k:(len(vocab_[j]) / len(vocab_[k])) < 2)
       last = i #findFirstSatisfying(j, i-1, lambda k:(len(vocab_[k]) / len(vocab_[j])) > 2)
       assert makePrefix(vocab_[first]) == lastPrefix
       assert makePrefix(vocab_[last-1]) == lastPrefix
       assert makePrefix(vocab_[last]) != lastPrefix
#       if last > 5000:
 #        print(vocab_[first:last])
  #       quit()
       relevant = torch.LongTensor([numerify(x) for x in vocab_[first:last]]).cuda()
       embedded = word_embeddings(relevant)
      # print(embedded.size())

       # non-capitalized version
       stoi_prefixes[prefix] = len(itos_prefixes)
       itos_prefixes.append(prefix)
       prefix_embeddings_matrix.append(embedded.mean(dim=0).data.clone())



       lastPrefix = prefix
       startPos = i


print(len(itos_prefixes))
assert stoi_prefixes[itos_prefixes[100]] == 100




word_embeddings_preview = torch.nn.Embedding(num_embeddings = len(itos_prefixes), embedding_dim = 200).cuda()
print(word_embeddings_preview.weight.data.size())

# only if not initializing from an attention model!
word_embeddings_preview.weight.data = torch.stack(prefix_embeddings_matrix, dim=0)

print(word_embeddings_preview.weight.data.size())



components_attention = [bilinear, word_embeddings_preview, char_embeddings]

def parameters():
 for c in components_lm:
   for param in c.parameters():
      yield param
 for c in components_attention:
   for param in c.parameters():
      yield param

optimizer = torch.optim.SGD(parameters(), lr = args.learning_rate)







def forward(batch, calculateAccuracy=False):
    texts = [[PAD] + [numerify(y) for y in x] + [PAD] for x in batch] # [:500]
    padding = [[stoi_characters["PAD"] for _ in range(args.previewLength)]]
    texts_preview_characters_num = [padding + [numerify_preview_characters(y) for y in x] + padding for x in batch] # [:500]
    texts_preview_numerified = [[PAD] + [numerify_preview(y) for y in x] + [PAD] for x in batch] # [:500]
    text_length = max([len(x) for x in texts])
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)
    texts =  torch.LongTensor(texts).cuda()


    for text in texts_preview_numerified:
       while len(text) < text_length:
          text.append(PAD)
    texts_preview_numerified = torch.LongTensor(texts_preview_numerified).cuda()



    mask = torch.FloatTensor([1 for _ in range(len(batch))]).cuda()
    masked = torch.LongTensor([SKIPPED]).cuda().expand(len(batch))
#    print(masked.size())
    hidden = None
    outputs = []

    attentionProbability_ = []
    attentionDecisions_ = []
    attentionLogit_ = []


#    texts = torch.LongTensor(texts).cuda().transpose(0,1)
 #   texts_num = texts

    maxLength = max(len(x) for x in texts_preview_characters_num)
    for x in texts_preview_characters_num:
       while len(x) < maxLength:
          x.append([stoi_characters["PAD"] for _ in range(args.previewLength)])
    texts_preview_characters_num = torch.LongTensor(texts_preview_characters_num).cuda()
    texts_preview_characters = char_embeddings(texts_preview_characters_num)
    texts_rnn = texts_preview_characters
    texts_preview_rnn = texts_rnn.mean(dim=2)


    texts_preview = word_embeddings_preview(texts_preview_numerified).detach()
#    print(texts_preview.size(), texts_preview_rnn.size())
    texts_preview = torch.cat([texts_preview, texts_preview_rnn], dim=2)



    assert not WITH_CONTEXT
    # Calculate the context-independent attention logits
    attention_logits_total = bilinear(texts_preview)
    # Iterate over the input

    attention_logits = attention_logits_total.squeeze(2) # batch, length
    attentionProbability = torch.nn.functional.sigmoid(attention_logits).transpose(0,1)
    attentionDecisions = torch.bernoulli(attentionProbability)
    mask = attentionDecisions.transpose(0,1)

    embedded_ = word_embeddings(torch.where(mask==1.0, texts, masked.unsqueeze(1))).transpose(0,1)
    outputs_reader, hidden = reader(embedded_, hidden)
    embedded = word_embeddings(texts).transpose(0,1)
    outputs_decoder, _ = reconstructor(embedded[:-1], hidden)
    if WITH_LM:
      # Collect target values for both surprisal and decoding loss
      targets = texts.transpose(0,1)
      targets = torch.cat([targets[1:], targets[1:]], dim=0)
      outputs_cat = output(torch.cat([outputs_reader, outputs_decoder], dim=0))
    else:
      # Collect target values for decoding loss
      targets = texts.transpose(0,1).contiguous()
      targets = targets[1:]
      outputs_cat = output(outputs_decoder)
    loss = crossEntropy(outputs_cat.view(-1, 50004), targets.view(-1)).view(outputs_cat.size()[0], outputs_cat.size()[1])

    attentionLogProbability = torch.nn.functional.logsigmoid(torch.where(mask == 1, attention_logits, -attention_logits)).t()

#    print(attentionProbability.size(), attentionDecisions.size(), attentionLogProbability.size(), attention_logits.size(), loss.size())
 #   quit()

    # At random times, print surprisals and reconstruction losses
    if random.random() < 0.1:
       print(len(texts), loss.size(), targets.size(), attentionProbability.size(), attentionDecisions.size())
       if WITH_LM:
         loss_reader = loss[:10, 0].cpu()
         loss_reconstructor = loss[51:61, 0].cpu()
       else:
         loss_reconstructor = loss[:10, 0].cpu()

       assert attentionProbability.size()[1] == args.batchSize
       assert attentionDecisions.size()[1] == args.batchSize
       attentionProbability_ = attentionProbability[:10, 0].cpu()
       attentionDecisions_ = attentionDecisions[:10, 0].cpu()
       print("\t".join(["Pos", "Word", "Pred", "Rec", "AttProb", "Att?"]))
       for j in range(10):
         try:
            if WITH_LM:
              print("\t".join([str(y) for  y in [j, batch[0][j]] +[round(float(x),4) for x in [loss_reader[j], loss_reconstructor[j], attentionProbability_[j], attentionDecisions_[j]]]]))
            else:
              print("\t".join([str(y) for  y in [j, batch[0][j]] +[round(float(x),4) for x in [-1, loss_reconstructor[j], attentionProbability_[j], attentionDecisions_[j]]]]))
            # Note that I'm using -1 for surprisal as this version of the model doesn't compute surprisal
         except IndexError:
            print(j, "IndexError")
#       quit()

    #print(attentionLogProbability.size())
    loss = loss.mean(dim=0)
    attentionDecisions = attentionDecisions.mean(dim=0)
    return loss, attentionLogProbability, attentionDecisions


fixationRunningAverageByCondition = [0.5,0.5]
rewardAverage = 10.0
lossAverageByCondition = [10.0, 10.0]

def backward(loss, action_logprob, fixatedFraction, printHere=True):
   global rewardAverage
   if random.random() < 0.99:
     batchSize = fixatedFraction.size()[0]
     fix1 = float(fixatedFraction.mean())
     fixationRunningAverageByCondition[0] = 0.99 * fixationRunningAverageByCondition[0] + (1-0.99) * fix1

     loss1 = float(loss.mean())
     lossAverageByCondition[0] = 0.99 * lossAverageByCondition[0] + (1-0.99) * loss1
     if printHere:
       print("FIXATION RATE", fixationRunningAverageByCondition, "REWARD", rewardAverage, "LossByCondition", lossAverageByCondition)
   optimizer.zero_grad()
   action_prob = torch.exp(action_logprob).clamp(min=0.0001, max=1-0.0001)
   oneMinusActionProb = 1-action_prob
   negEntropy = (action_prob * action_logprob + oneMinusActionProb * oneMinusActionProb.log()).mean()
   reward = (loss.detach() + args.LAMBDA * fixatedFraction)
   assert loss.size()[0] == action_logprob.size()[1], (loss.size(), action_logprob.size())
   loss_ = args.REWARD_FACTOR * (action_logprob * (reward - rewardAverage)).mean() + loss.mean() + args.ENTROPY_WEIGHT * negEntropy
   loss_.backward()
   rewardAverage = 0.99 * rewardAverage + (1-0.99) * float(reward.mean())
   if args.clip_type != "None":
#      print("NROM MAX", max(p.grad.detach().abs().max().cpu() for p in parameters()))
      torch.nn.utils.clip_grad_norm_(parameters(), args.clip_bound, norm_type=args.clip_type)
   optimizer.step()

#my_save_path = f"/jagupard18/scr1/mhahn/{__file__}_{args.myID}.ckpt"
my_save_path = f"SCRATCH/CHECKPOINTS/{__file__}_{args.myID}.ckpt"

import time


learning_rate=args.learning_rate

def SAVE():
       torch.save({"devRewards" : devRewards, "args" : args, "components_lm" : [x.state_dict() for x in components_lm], "components_attention" : [x.state_dict() for x in components_attention], "learning_rate" : learning_rate, "itos_characters" : itos_characters}, my_save_path)



devLosses = []
lossRunningAverage = 6.4
devAccuracies = []
devRewards = []
noImprovement = 0
for epoch in range(10):
  
  if epoch >= 1:
    validLoss = []
    examplesNumber = 0
    validAccuracy = []
    validReward = []
    validAccuracyPerCondition = [0.0, 0.0]
    validFixationsPerCondition = [0.0, 0.0]
    for batch in  loadQACorpus(args.corpus, "test", args.batchSize, permuteEntities=True):
     with torch.no_grad():
       loss, action_logprob, fixatedFraction = forward(batch, calculateAccuracy = True)
       if loss is None:
          continue
       reward = float((loss.detach() + args.LAMBDA * fixatedFraction).mean())
       loss = float(loss.mean())
       print("VALID", loss, examplesNumber, reward, epoch)
#     print(accuracy)
     #print(fixatedFraction)
     fixationsCond1 = float(fixatedFraction.sum())
     validFixationsPerCondition[0] += fixationsCond1


     validLoss.append(float(loss)*len(batch))
     validReward.append(reward*len(batch))
     examplesNumber += len(batch)
    devLosses.append(sum(validLoss)/examplesNumber)
    devRewards.append(sum(validReward)/examplesNumber)
    validFixationsPerCondition[0] /= examplesNumber
    print(f"SCRATCH/results/accuracy_{__file__}_{args.myID}.txt")
    with open(f"SCRATCH/results/accuracy_{__file__}_{args.myID}.txt", "w") as outFile:
       print(args, file=outFile)
       print(devAccuracies, file=outFile)
       print(devLosses, file=outFile)
       print(devRewards, file=outFile)
       print(fixationRunningAverageByCondition[0], fixationRunningAverageByCondition[1], "savePath="+my_save_path, file=outFile)
       print(rewardAverage, "\t", validAccuracyPerCondition[0], validAccuracyPerCondition[1], "\t", validFixationsPerCondition[0], validFixationsPerCondition[1], file=outFile)
    if len(devRewards) >1 and devRewards[-1] > devRewards[-2]:
       learning_rate *= 0.8
       optimizer = torch.optim.SGD(parameters(), lr = learning_rate)
       noImprovement += 1
    elif len(devRewards) > 1 and devRewards[-1] == min(devRewards):
       SAVE()
  
    #   noImprovement = 0
    if noImprovement > 1:
      print("End training, no improvement for 2 epochs")
      break
  words = 0
  timeStart = time.time()
  counter = 0
  for batch in loadQACorpus(args.corpus, "train", args.batchSize):
    counter += 1
    printHere = (counter % 5) == 0
    loss, action_logprob, fixatedFraction = forward(batch)
    if loss is None:
       continue
    backward(loss, action_logprob, fixatedFraction, printHere=printHere)
    loss = float(loss.mean())
    lossRunningAverage = 0.99 *lossRunningAverage + (1-0.99) * float(loss)
    words += len(batch)
    if printHere:
#       print(devAccuracies)
       print(devLosses)
       print(devRewards)
       print(float(loss), lossRunningAverage, args, __file__, counter)
       print(words/(time.time()-timeStart), "questions per second")
    if noImprovement == 0 and counter % 10000 == 0:
       SAVE()

