# train_attention_6.py: THIS one works nice and fast

__file__ = __file__.split("/")[-1]

import random
import argparse

parser = argparse.ArgumentParser()


parser.add_argument('--corpus', type=str, default="cnn")
parser.add_argument('--domain', type=str, default=random.choice(["bio", "phys"]))
parser.add_argument('--batchSize', type=int, default=random.choice([64]))
parser.add_argument('--learning_rate', type=float, default=random.choice([0.001, 0.01, 0.05, 0.1, 0.2, 0.5, 1.0]))
parser.add_argument('--dropout', type=float, default=0.0)
parser.add_argument('--LOAD_CKPT', type=int, default=random.randint(1000,100000000))
parser.add_argument('--myID', type=int, default=random.randint(1000,100000000))
parser.add_argument('--SEQUENCE_LENGTH', type=int, default=50)
parser.add_argument('--LAMBDA', type=float, default=random.choice([0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.25, 2.5, 2.75, 3]))
parser.add_argument('--REWARD_FACTOR', type=float, default=random.choice([0.1]))
parser.add_argument('--ENTROPY_WEIGHT', type=float, default=random.choice([0.0005, 0.005, 0.005])) #random.choice([0.0001, 0.001, 0.01, 0.1]))
parser.add_argument('--previewLength', type=int, default=random.choice([4]))
parser.add_argument('--lengthUntil', type=int, default=random.choice([0]))
#parser.add_argument('--load_from', type=int, default=random.choice([23878455,5439711]))

args = parser.parse_args()

args.clip_type = random.choice([2, "inf"]) # 2
args.clip_bound = random.choice([2, 5, 10, 15])


SEQUENCE_LENGTH = args.SEQUENCE_LENGTH
assert SEQUENCE_LENGTH==50

vocabulary = [x.split("\t") for x in open(f"vocabularies/german.txt", "r").read().strip().split("\n")]
itos = [x[1] for x in vocabulary]
stoi = dict([(x[1], int(x[0])) for x in vocabulary])

itos_characters = set()
for x in itos:
   for y in x:
      itos_characters.add(y)
      itos_characters.add(y.lower())
      itos_characters.add(y.upper())
itos_characters = ["OOV", "PAD", "EOS", "SOS"] + (sorted(list(itos_characters))[:100])
stoi_characters = dict(list(zip(itos_characters, range(len(itos_characters)))))
print(stoi_characters)


def unique(l):
    s = set()
    r = []
    for x in l:
      if x not in s:
        s.add(x)
        r.append(x)
    return r

answerDistribution = [0, [0 for _ in range(600)]]
 

import gzip

assert args.corpus in ["wiki_spektrum"]

def loadQACorpus(corpus, partition, batchSize, permuteEntities=True):
   global HEADER
   HEADER = {"File" : 0, "Index" : 1, "Token" :2}
   for file_ in [x for x in sorted(glob.glob("SCRATCH/potec/*.txt")) if not x.split("/")[-1.startswith("all_")]:
     with open(file_, "r") as inFile:
        text = inFile.read().strip().split(" ")
        concatenated = [[file_.split("/")[-1], index, token.replace("\n", " ")] for index, token in enumerate(text)]
        partitions = []
        for i in range(int(len(concatenated)/SEQUENCE_LENGTH)+1):
          r = concatenated[i*SEQUENCE_LENGTH:(i+1)*SEQUENCE_LENGTH]
          if len(r) > 0:
            partitions.append(r)
        for i in range(int(len(partitions)/args.batchSize)+1):
          r = partitions[i*args.batchSize:(i+1)*args.batchSize]
          if len(r) > 0:
            yield r
OOV = 2
SKIPPED = 1
PAD = 0
PLACEHOLDER = 3

#training = 


def lengthBin(word):
  if len(word) < args.lengthUntil:
     return len(word)
  return args.lengthUntil


def makePrefix(x):
   preview = x[:args.previewLength]+"_"+str(lengthBin(x))
   return preview


def numerify(token):
   if token == "@placeholder":
      return PLACEHOLDER
   elif token not in stoi or stoi[token] >= 50000:
      return OOV
   else:
      return stoi[token]+4

def numerify_preview_characters(token):
#   token = token.replace("—", "-").replace('”', '"').replace('“', '"').replace("»", '"').replace("…", "...").replace("‘", "'").replace("’", "'").replace("!", "!").replace("–", "-") # this relates to DailyMail vs CNN
   token = token[:args.previewLength]
   #for x in token:
  #   if x not in stoi_characters:
 #       print("UNKNOWN", x)
   return [stoi_characters.get(x, stoi_characters["OOV"]) for x in token] + [stoi_characters["PAD"] for _ in range(args.previewLength-len(token))]




def numerify_preview(token):
#   token = token.lower()
#   token = token.replace("—", "-").replace('”', '"').replace('“', '"').replace("»", '"').replace("…", "...").replace("‘", "'").replace("’", "'").replace("!", "!").replace("–", "-") # this relates to DailyMail vs CNN
   preview = makePrefix(token)
   if preview not in stoi_prefixes:
      return OOV
   else:
      return stoi_prefixes[preview]


import torch




word_embeddings = {}
char_embeddings = {}
reader = {}
reconstructor = {}
output = {}
components_lm = {}
components_attention = {}
bilinear = {}
word_embeddings_preview = {}

word_embeddings["bio"] = torch.nn.Embedding(num_embeddings = 50000+4, embedding_dim = 200).cuda()
char_embeddings["bio"] = torch.nn.Embedding(num_embeddings = 100+4, embedding_dim = 128).cuda()
word_embeddings["phys"] = torch.nn.Embedding(num_embeddings = 50000+4, embedding_dim = 200).cuda()
char_embeddings["phys"] = torch.nn.Embedding(num_embeddings = 100+4, embedding_dim = 128).cuda()


########################
reader["bio"] = torch.nn.LSTM(200, 1024, 1).cuda()
reconstructor["bio"] = torch.nn.LSTM(200, 1024, 1).cuda()
output["bio"] = torch.nn.Linear(1024, 50000 + 4).cuda()
reader["phys"] = torch.nn.LSTM(200, 1024, 1).cuda()
reconstructor["phys"] = torch.nn.LSTM(200, 1024, 1).cuda()
output["phys"] = torch.nn.Linear(1024, 50000 + 4).cuda()


input_dropout = torch.nn.Dropout(args.dropout)

nllLoss = torch.nn.NLLLoss(reduction="none", ignore_index=PAD)
crossEntropy = torch.nn.CrossEntropyLoss(reduction="none", ignore_index=PAD)

components_lm["bio"] = [word_embeddings["bio"], reader["bio"], reconstructor["bio"], output["bio"]]
components_lm["phys"] = [word_embeddings["phys"], reader["phys"], reconstructor["phys"], output["phys"]]

ONLY_REC = True
WITH_CONTEXT = False
WITH_LM = False

if WITH_CONTEXT:
  # Use a bilinear module to combine word embedding with context information
  bilinear["bio"] = torch.nn.Bilinear(1024, 328, 1).cuda()
else:
  # Use a linear module for derive attention logit from word embedding
  bilinear["bio"] = torch.nn.Linear(328, 1).cuda()
if WITH_CONTEXT:
  # Use a bilinear module to combine word embedding with context information
  bilinear["phys"] = torch.nn.Bilinear(1024, 328, 1).cuda()
else:
  # Use a linear module for derive attention logit from word embedding
  bilinear["phys"] = torch.nn.Linear(328, 1).cuda()





def findFirstSatisfying(startIncl, endIncl, condF):
   if condF(startIncl):
     return startIncl
   i = endIncl
   while True:
      if startIncl == i:
         return i
      if startIncl+1 == i:
         return i
      med = int((startIncl + i)/2)
      if condF(med):
          i = med
      else:
         startIncl = med
   

associated = {}

vocab_ = sorted(itos[:50000], key=lambda x:(makePrefix(x), len(x)))
print(vocab_[:100])
lastPrefix = ""
startPos = 0
notDone = set(range(50005))
#word_embeddings_preview.weight.data = word_embeddings.weight.data.clone().mean(dim=0).unsqueeze(0).expand(word_embeddings.weight.data.size()[0], -1)

#word_embeddings_preview.weight.data.zero_()


itos_prefixes = ["PAD", "SKIPPED", "OOV", "PLACEHOLDER"]
stoi_prefixes = {x : itos_prefixes.index(x) for x in itos_prefixes}
#prefix_embeddings_matrix = [word_embeddings.weight.data[i].clone() for i in range(len(itos_prefixes))]

startPos = 0
lastPrefix = makePrefix(vocab_[0])
for i in range(1,len(vocab_)):
   prefix = makePrefix(vocab_[i])
   if prefix in stoi_prefixes:
     continue
   elif prefix != lastPrefix:
       first = startPos #findFirstSatisfying(startPos, j, lambda k:(len(vocab_[j]) / len(vocab_[k])) < 2)
       last = i #findFirstSatisfying(j, i-1, lambda k:(len(vocab_[k]) / len(vocab_[j])) > 2)
       assert makePrefix(vocab_[first]) == lastPrefix
       assert makePrefix(vocab_[last-1]) == lastPrefix
       assert makePrefix(vocab_[last]) != lastPrefix
#       if last > 5000:
 #        print(vocab_[first:last])
  #       quit()
#       relevant = torch.LongTensor([numerify(x) for x in vocab_[first:last]]).cuda()
#       embedded = word_embeddings(relevant)
      # print(embedded.size())

       # non-capitalized version
       stoi_prefixes[prefix] = len(itos_prefixes)
       itos_prefixes.append(prefix)
#       prefix_embeddings_matrix.append(embedded.mean(dim=0).data.clone())



       lastPrefix = prefix
       startPos = i


print(len(itos_prefixes))
assert stoi_prefixes[itos_prefixes[100]] == 100


DOMAINS = ["bio", "phys"]

word_embeddings_preview["bio"] = torch.nn.Embedding(num_embeddings = len(itos_prefixes), embedding_dim = 200).cuda()
word_embeddings_preview["phys"] = torch.nn.Embedding(num_embeddings = len(itos_prefixes), embedding_dim = 200).cuda()


optimizer = {}


components_attention["bio"] = [bilinear["bio"], word_embeddings_preview["bio"], char_embeddings["bio"]]
components_attention["phys"] = [bilinear["phys"], word_embeddings_preview["phys"], char_embeddings["phys"]]

def parameters(domain):
 for c in components_lm[domain]:
   for param in c.parameters():
      yield param
 for c in components_attention[domain]:
   for param in c.parameters():
      yield param

optimizer["bio"] = torch.optim.SGD(parameters("bio"), lr = args.learning_rate)
optimizer["phys"] = torch.optim.SGD(parameters("phys"), lr = args.learning_rate)

import glob
import random

state = torch.load(f"SCRATCH/CHECKPOINTS/train_attention4_CharPreview_Finetune2.py_{args.LOAD_CKPT}.ckpt")

print("args", state["args"])
args.LAMBDA = state["args"].LAMBDA
assert args.LAMBDA == state["args"].LAMBDA
print(state["devRewards"])
if len(state["devRewards"]) < 2:
   print("Quitting")
   quit()
for domain in DOMAINS:
  for i in range(len(components_lm[domain])):
     components_lm[domain][i].load_state_dict(state[f"components_lm_{domain}"][i])
  for i in range(len(components_attention[domain])):
     components_attention[domain][i].load_state_dict(state[f"components_attention_{domain}"][i])
  





def forward(batch, domain, calculateAccuracy=False):
    texts = [[PAD] + [numerify(y[HEADER["Token"]]) for y in x] + [PAD] for x in batch] # [:500]
    padding = [[stoi_characters["PAD"] for _ in range(args.previewLength)]]
    texts_preview_characters_num = [padding + [numerify_preview_characters(y[HEADER["Token"]]) for y in x] + padding for x in batch] # [:500]
    texts_preview_numerified = [[PAD] + [numerify_preview(y[HEADER["Token"]]) for y in x] + [PAD] for x in batch] # [:500]
    text_length = max([len(x) for x in texts])
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)
    texts =  torch.LongTensor(texts).cuda()


    for text in texts_preview_numerified:
       while len(text) < text_length:
          text.append(PAD)
    texts_preview_numerified = torch.LongTensor(texts_preview_numerified).cuda()



    mask = torch.FloatTensor([1 for _ in range(len(batch))]).cuda()
    masked = torch.LongTensor([SKIPPED]).cuda().expand(len(batch))
#    print(masked.size())
    hidden = None
    outputs = []

    attentionProbability_ = []
    attentionDecisions_ = []
    attentionLogit_ = []


#    texts = torch.LongTensor(texts).cuda().transpose(0,1)
 #   texts_num = texts

    maxLength = max(len(x) for x in texts_preview_characters_num)
    for x in texts_preview_characters_num:
       while len(x) < maxLength:
          x.append([stoi_characters["PAD"] for _ in range(args.previewLength)])
    texts_preview_characters_num = torch.LongTensor(texts_preview_characters_num).cuda()
    texts_preview_characters = char_embeddings[domain](texts_preview_characters_num)
    texts_rnn = texts_preview_characters
    texts_preview_rnn = texts_rnn.mean(dim=2)


    texts_preview = word_embeddings_preview[domain](texts_preview_numerified).detach()
#    print(texts_preview.size(), texts_preview_rnn.size())
    texts_preview = torch.cat([texts_preview, texts_preview_rnn], dim=2)




    if not WITH_CONTEXT:
      # Calculate the context-independent attention logits
      attention_logits_total = bilinear[domain](texts_preview)
    # Iterate over the input
    for i in range(texts.size()[1]-1):
#       print(mask.size(), texts[:,i].size(), masked.size())
       embedded_ = word_embeddings[domain](torch.where(mask==1.0, texts[:,i], masked)).unsqueeze(0)
 #      print(embedded_.size())
       _, hidden = reader[domain](embedded_, hidden)
       outputs.append(hidden[0])
       #print("HIDDEN", hidden.size())
       #print(output_here.size())
       if WITH_CONTEXT:
         # Calculate the context-dependent attention logits
         embedded_ = texts_preview[:,i+1]
         attention_logits = bilinear[domain](hidden[0].squeeze(0), embedded_).squeeze(1)
       else:
         attention_logits = attention_logits_total[:,i+1].squeeze(1)
       #print(attention_logits.size())
       attentionProbability = torch.nn.functional.sigmoid(attention_logits)
       if WITH_CONTEXT:
         attentionDecisions = torch.bernoulli(torch.clamp(attentionProbability, min=0.01, max=0.99))
       else:
         attentionDecisions = torch.bernoulli(attentionProbability)
       mask = attentionDecisions
       attentionProbability_.append(attentionProbability)
       attentionDecisions_.append(attentionDecisions)
       attentionLogit_.append(attention_logits)

    attentionProbability = torch.stack(attentionProbability_, dim=0)
    attentionDecisions = torch.stack(attentionDecisions_, dim=0)
    attentionLogit = torch.stack(attentionLogit_, dim=0)
    if WITH_LM:
      # Collect outputs from the reader module to compute surprisal
      outputs_reader = torch.cat(outputs, dim=0)
    embedded = word_embeddings[domain](texts).transpose(0,1)
    outputs_decoder, _ = reconstructor[domain](embedded[:-1], hidden)
    if WITH_LM:
      # Collect target values for both surprisal and decoding loss
      targets = texts.transpose(0,1)
      targets = torch.cat([targets[1:], targets[1:]], dim=0)
      outputs_cat = output(torch.cat([outputs_reader, outputs_decoder], dim=0))
    else:
      # Collect target values for decoding loss
      targets = texts.transpose(0,1).contiguous()
      targets = targets[1:]
      outputs_cat = output[domain](outputs_decoder)
    loss = crossEntropy(outputs_cat.view(-1, 50004), targets.view(-1)).view(outputs_cat.size()[0], outputs_cat.size()[1])

    text_from_batch = []


    if True:
       sequenceLengthHere= text_length-2
       loss_reader = loss.cpu()
       attentionProbability_ = attentionProbability.cpu()
       attentionDecisions_ = attentionDecisions.cpu()
       for batch_ in range(loss.size()[1]):
        for pos in range(loss.size()[0]):
         try:
#           print(batch[batch_][pos])
           lineForWord = batch[batch_][pos]
           text_from_batch.append([str(y) for  y in [pos, lineForWord[HEADER["Token"]], lineForWord[HEADER["File"]], lineForWord[HEADER["Index"]]] +[round(float(x),4) for x in [loss_reader[pos,batch_], attentionProbability_[pos,batch_], attentionDecisions_[pos, batch_]]]])
         except IndexError:
           pass
    loss = loss.mean(dim=0)
    print("return", len(text_from_batch))
    return loss, text_from_batch


fixationRunningAverageByCondition = [0.5,0.5]
rewardAverage = 10.0
lossAverageByCondition = [10.0, 10.0]

import time


learning_rate=args.learning_rate

devLosses = []
lossRunningAverage = 6.4
noImprovement = 0

concatenated = []
with open(f"SCRATCH/predictions/{__file__}_{args.LOAD_CKPT}_{args.domain}", "w") as outFile:
    validLoss = []
    examplesNumber = 0
    TEXT_ = []
    batches = list(loadQACorpus(args.corpus, None, args.batchSize, permuteEntities=True))
    print("batches", len(batches))
    for batch in batches:
     with torch.no_grad():
       loss, TEXT = forward(batch, domain=args.domain, calculateAccuracy = True)
       loss = float(loss.mean())
       print("VALID", loss, examplesNumber)
       for x in TEXT:
         TEXT_.append(x)

     validLoss.append(float(loss)*len(batch))
     examplesNumber += len(batch)
     count = 0
    for x in TEXT_:
       print("\t".join(x), file=outFile)

