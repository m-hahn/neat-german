import glob

SCRIPT = __file__.replace("collectLogs_", "")


logs = glob.glob(f"SCRATCH/results/accuracy_{SCRIPT}_*.txt")
print(logs)
results = []
for f in logs:
  with open(f, "r") as inFile:
    data = inFile.read().strip().split("\n")
 # print(data, len(data))
  args, devAccuracies, devLosses, devRewards, fixationRates, rewards = data
  args = dict([x.split("=") for x in args[10:-1].split(", ")])
#  print(args["LAMBDA"])
  fixationRate = rewards.split("\t")[2].strip().split(" ")[0]
#  print(devRewards)
  results.append(([args["myID"], args["LAMBDA"], args["ENTROPY_WEIGHT"], fixationRate, devRewards.split(", ")[-1].strip("[]"), str(len(devRewards.split(", "))), str(args)]))
results = sorted(results, key=lambda x:(x[1], x[2], x[4]))
with open(f"logs/{__file__}.tsv", "w") as outFile:
 print("\t".join(["ModelID", "alpha", "entropy_weight", "FixationRate", "Epochs"]), file=outFile)
 for r in results:
    print("\t".join(r), file=outFile)
