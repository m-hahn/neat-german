import glob
files = glob.glob("SCRATCH/CHECKPOINTS/train_attention4_CharPreview_Finetune_Long2.py_*.ckpt")
import subprocess
for f in files:
   id_ = f[f.rfind("_")+1:-5]
   for domain in ["phys", "bio"]:
     subprocess.call(["python3", "train_attention4_CharPreview_Finetune_Long2_GetPredictions.py", "--LOAD_CKPT="+id_, "--corpus=wiki_spektrum", "--domain="+domain])
