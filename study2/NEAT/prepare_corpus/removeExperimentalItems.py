# Verify that the texts come from the test sets, which are not used in training the neural models in this work

import os
files = os.listdir("../../analysis/experiment_data/prepare_experiment/selectedTexts/CORPUS/")
forbiddenURLs = []
for f in files:
   print(f)
   with open("../../analysis/experiment_data/prepare_experiment/selectedTexts/CORPUS/"+f, "r") as inFile:
        next(inFile)
        forbiddenURLs.append(next(inFile).strip())
print(forbiddenURLs)

forbiddenURLs = set(forbiddenURLs)
for corpus in ["cnn", "dailymail"]:
 for partition in ["test", "validation", "training"]:
   print("======")
   print(partition)
   with open(f"BASE/STUDY2/DEEPMIND_CORPUS_CHO/{corpus}_{partition}.txt", "r") as inFile:      
      for line in inFile:
        url = (line.split("##########")[0])
        if url in forbiddenURLs:
           print(url)
           forbiddenURLs.remove(url)
print(forbiddenURLs)
