import os

assert False, "Don't do this again by accident!"

from collections import defaultdict
for corpus in ["cnn", "dailymail"]:
   vocabulary = defaultdict(int)
   for partition in ["test", "validation", "training"]:
     print(corpus, partition)
     with open(f'/john5/scr1/mhahn/CHO/{corpus}/questions/{partition}.txt', "w") as outFile:
       PATH = f'/john5/scr1/mhahn/CHO/{corpus}/questions/{partition}/'
       files = os.listdir(PATH)
       for name in files:
         with open(PATH+name) as inFile:
           data = inFile.read().strip().split("\n\n")
#           data = inFile.read().strip().replace("\n\n", "##########").replace("\n", "@@@@")
           assert (len(data)) == 5
           assert "\n" not in data[0]
           assert "\n" not in data[1]
           assert "\n" not in data[2]
           assert "\n" not in data[3]
           data[4] = data[4].replace("\n", "@@@@@")
           #print(data)
           #quit()
           print("##########".join(data), file=outFile)
           for text in [data[0], data[1]]:
             for word in text.split(" "):
               if len(word) == 0:
                   continue
               vocabulary[word] += 1
   vocabulary_list = list(vocabulary.items())
   vocabulary_list = sorted(vocabulary_list, key=lambda x:x[1], reverse=True)
   with open(f"vocabularies/{corpus}_anonymized.txt", "w") as outFile:
       for i in range(min(500000, len(vocabulary_list))):
           print(f"{str(i)}\t{vocabulary_list[i][0]}\t{str(vocabulary_list[i][1])}", file=outFile)
