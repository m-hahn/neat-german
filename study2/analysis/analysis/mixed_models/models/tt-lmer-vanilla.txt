
\begin{table}$
\begin{center}$
\begin{tabular}${l c}$
\hline
 & Model 1 \\
\hline
(Intercept)                                          & 276.30 &  (7.61) &  $^{***}$  \\
PositionText                               & -9.87 &  (1.15) &  $^{***}$   \\
Condition                             & 67.52 & (15.10) &  $^{***}$   \\
LogWordFreq                                 & -40.84 &  (1.26) &  $^{***}$  \\
IsNamedEntity                               & 4.96 &  (4.38) &            \\
IsCorrectAnswer.Resid                                & 8.21 & (16.81) &            \\
Surprisal.Resid                                      & 3.39 &  (0.45) &  $^{***}$    \\
WordLength.Resid                          & 13.87 &  (0.58) &  $^{***}$   \\
Condition:IsCorrectAnswer.Resid       & -186.23 & (12.06) &  $^{***}$ \\
Condition:LogWordFreq        & -18.46 &  (1.52) &  $^{***}$  \\
LogWordFreq:IsNamedEntity          & -44.01 &  (3.61) &  $^{***}$  \\
PositionText:LogWordFreq          & 6.11 &  (1.24) &  $^{***}$    \\
Condition:WordLength.Resid & 5.11 &  (0.73) &  $^{***}$    \\
PositionText:IsCorrectAnswer.Resid         & -115.03 & (14.20) &  $^{***}$ \\
LogWordFreq:IsCorrectAnswer.Resid           & -96.79 & (12.30) &  $^{***}$  \\
IsCorrectAnswer.Resid:Surprisal.Resid                & -16.07 &  (3.03) &  $^{***}$  \\
PositionText:IsNamedEntity        & -18.97 &  (3.52) &  $^{***}$  \\
IsCorrectAnswer.Resid:WordLength.Resid    & 18.58 &  (4.56) &  $^{***}$   \\
IsNamedEntity:WordLength.Resid   & 6.27 &  (1.53) &  $^{***}$    \\
IsNamedEntity:Surprisal.Resid               & -3.62 &  (1.01) &  $^{***}$   \\
\hline
AIC                                                  & 835411.76                \\
BIC                                                  & 835619.77                \\
Log Likelihood                                       & -417682.88               \\
Num. obs.                                            & 62543                    \\
Num. groups: tokenID                                 & 4984                     \\
Num. groups: Participant                             & 22                       \\
Var: tokenID (Intercept)                             & 3243.75                  \\
Var: Participant (Intercept)                         & 1228.99                  \\
Var: Residual                                        & 34866.21                 \\
\hline
\multicolumn{2}${l}${\scriptsize{ &  $^{***}$p<0.001;  &  $^{**}$p<0.01;  &  $^{*}$p<0.05}$}$
\end{tabular}$
\caption{Statistical models}$
\label{table:coefficients}$
\end{center}$
\end{table}$
