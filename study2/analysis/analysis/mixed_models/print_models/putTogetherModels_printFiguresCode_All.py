assert False, "Caution: The output file has bveen manually altered. Don't overwrite unless you want to!"


def printSig(beta, level):
    if "*" in level:
        return "\\textbf{"+str(beta)+"}"
    else:
        return str(beta)


def printHighlight(x,y):
    if "nterc" not in y and "\\textbf" in x: #  or "CorrectAnswer" in y or "NamedEntity" in y
        return "\\textcolor{blue}{"+x+"}"
    else:
        return x

def formatLine(name, line):
    level = line[2]
    return [printHighlight(printSig(line[0], level), name), line[1]]

models = {x : open(f"models/{x}-lmer-vanilla.txt", "r").read().strip().split("\n") for x in ["ff", "fp", "tt"]}
models = {x : models[x][5:-16] for x in models}
models["FIXATED"] = open("models/FIXATED-lmer-BERNOULLI-brm.txt", "r").read().strip().split("\n")
#print(models)
#print(models["ff"])
predictors = set()
for x in models:
  if "FIXATED" in x:
     result = {}
     print("FIXATED", models[x])
     for line in models[x]:
        print(line)
        line= [x.strip() for x in line.split("\t")]
        predictor = line[0].replace("b_", "").replace(".Centered", "").replace("HumanPosition", "PositionText").replace(".dummy", "").replace("ExperimentTokenLength", "WordLength").strip().replace(".Resid", "")
        if predictor == "Intercept":
           predictor = "(Intercept)"
        predictors.add(predictor)
        level = float(line[3])
        level = min(level, (1-level))
        level = "$^{" + "*"*(0 if level > 0.05 else (1 if level > 0.01 else (2 if level > 0.005 else 3))) + "}$"
        result[predictor] = [str(y) for y in [printHighlight(printSig(round(float(line[1]),2), level), predictor), "("+str(round(float(line[2]),2))+")"]]
     models[x] = result
  else:
    assert models[x][0] == '\\hline'
    assert models[x][-1] == '\\hline'
    models[x]=models[x][1:-1]
    models[x] = [[z.strip() for z in y.replace("\\", "").split("&")] for y in models[x]]
    models[x] = {y[0].strip().replace(".Resid", "") : formatLine(y[0], y[1:]) for y in models[x]}
    for y in models[x]:
        predictors.add(y)
    print(models[x])
predictors_main = sorted([x for x in predictors if ":" not in x])
predictors_other = sorted([x for x in predictors if ":" in x and "Condition" not in x])
predictors_cond = sorted([x for x in predictors if ":" in x and "Condition" in x])






def flatten(y):
    r = []
    for x in y:
        for z in x:
            r.append(z)
    return r

def sameLengthForEachEntry(l):
    for i in range(len(l)):
        if len(l[i]) < 8:
            l[i] += " "*(8-len(l[i]))
    return l


def padEffect(x):
    return x+" "*(30-len(x))

measures = ["ff", "fp", "tt", "FIXATED"]

human_eff = { "IsCorrectAnswer" : 'answer status',
          "Surprisal" : 'surprisal',
          "WordLength" : "word length",
          "IsNamedEntity" : "named entity status",
          "LogWordFreq" : "log word frequency",
          "PositionText" : 'text position',
          "Condition" : 'condition'
          }

def formatMeasures(l):
    l = [["first fixation duration", "first pass duration", "total time", "fixations"][i] for i in l]
    if len(l) == 1:
        return l[0]
    else:
        return ", ".join(l[:-1])+" and "+l[-1]

def continuous(x):
    if x in ["IsCorrectAnswer", "IsNamedEntity", "Condition"]:
        return False
    return True

abbrev = { "IsCorrectAnswer" : 'corrans',
          "Surprisal" : 'surp',
          "WordLength" : "wlen",
          "IsNamedEntity" : "ne",
          "LogWordFreq" : "logwf",
          "PositionText" : 'pos',
          "Condition" : 'cond'
          }


print(predictors)
with open(f"output/{__file__}.tex", "w") as outFile:
 with open(f"output/{__file__}_vizCode.txt", "w") as outFile2:
  for group in [predictors_main, predictors_other, predictors_cond]:
    for effect in group:
        effects = [sameLengthForEachEntry(models[x].get(effect, ["", ""])) for x in ["ff", "fp", "tt", "FIXATED"]]
        if "blue" in effects[2][0] and group == predictors_other:
            for i in [0, 1, 3]:
                effects[i][0] = effects[i][0].replace("\\textcolor{blue}", "")
        measuresWithEffect = [i for i in range(4) if "blue" in effects[i][0]]
        if len(measuresWithEffect) == 0:
            continue
        print(effect, measuresWithEffect)
        print(effect+", "+(", ".join([["FirstFixation", "FirstPass", "TotalTime", "FixationRate"][i] for i in measuresWithEffect])), file=outFile2)
        effect = effect.split(":")
        print("", file=outFile)
        print("", file=outFile)
        print("\\begin{figure}", file=outFile)
        print("\\begin{center}", file=outFile)
        for i in measuresWithEffect:
            if measuresWithEffect.index(i) % 2 == 0:
                print("", file=outFile)
            print("\includegraphics[width=.5\\textwidth]{../study2/analysis/analysis/visualization-clip-avg/figures/"+"-".join(effect)+"-"+measures[i]+".pdf}%", file=outFile)
        print("\end{center}", file=outFile)
        if len(effect) == 2:
          print("        \caption{Interaction between "+human_eff[effect[0]]+" and "+human_eff[effect[1]]+" in "+formatMeasures(measuresWithEffect)+".", file=outFile)
        else:
          print("        \caption{Effect of "+human_eff[effect[0]]+" in "+formatMeasures(measuresWithEffect)+".", file=outFile)
        if "IsCorrectAnswer" in effect:
            print("We only show datapoints belonging to named entities, as answer status is only relevant for those. ", file=outFile)
        if any([continuous(x) for x in effect]):
           print(" We show GAM-smoothed means with 95\% confidence intervals.", file=outFile)
        print("}", file=outFile)
        print("\label{fig:"+":".join([abbrev[x] for x in effect])+"}", file=outFile)
        print("\end{figure}", file=outFile)
              

  # IsNamedEntity: FirstPass
  # IsNamedEntity:WordLength, FirstFixation, TotalTime, FixationRate
  # IsNamedEntity:Surprisal, FirstPass, TotalTime
  # LogWordFreq:IsCorrectAnswer, FirstPass, TotalTime
  # LogWordFreq:IsNamedEntity, FirstPass, TotalTime
  # PositionText:IsCorrectAnswer, TotalTime
  # PositionText:IsNamedEntity, TotalTime
  # Condition:IsCorrectAnswer, TotalTime
  # Condition:IsNamedEntity, FirstPass, TotalTime
  # Condition:LogWordFreq, FirstPass, TotalTime, FixationRate
  # Condition:WordLength, FirstFixation, FirstPass, TotalTime, FixationRate
  # IsCorrectAnswer:WordLength, TotalTime




