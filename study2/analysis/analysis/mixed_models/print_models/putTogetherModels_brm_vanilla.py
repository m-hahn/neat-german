
models = {x : open(f"models/{x}-lmer-VANILLA-brm.txt", "r").read().strip().split("\n") for x in ["ff", "fp", "tt"]}
models["FIXATED"] = open("models/FIXATED-lmer-BERNOULLI-brm.txt", "r").read().strip().split("\n")
#print(models)
#print(models["ff"])
predictors = set()
for x in models:
     result = {}
     print(x, models[x])
     for line in models[x]:
        print(line)
        line= [x.strip() for x in line.split("\t")]
        predictor = line[0].replace("b_", "").replace(".Centered", "").replace("HumanPosition", "PositionText").replace(".dummy", "").replace("ExperimentTokenLength", "WordLength").strip().replace(".Resid", "")
        if predictor == "Intercept":
           predictor = "(Intercept)"
        predictors.add(predictor)
        level = float(line[3])
        level = min(level, (1-level))
        level = "$^{" + "*"*(0 if level > 0.05 else (1 if level > 0.01 else (2 if level > 0.005 else 3))) + "}$"
        result[predictor] = [str(y) for y in [round(float(line[1]),2), "("+str(round(float(line[2]),2))+")", level]]
     models[x] = result
predictors_main = sorted([x for x in predictors if ":" not in x])
predictors_other = sorted([x for x in predictors if ":" in x and "Condition" not in x])
predictors_cond = sorted([x for x in predictors if ":" in x and "Condition" in x])

def flatten(y):
    r = []
    for x in y:
        for z in x:
            r.append(z)
    return r

def sameLengthForEachEntry(l):
    for i in range(len(l)):
        if len(l[i]) < 8:
            l[i] += " "*(8-len(l[i]))
    return l


def padEffect(x):
    return x+" "*(30-len(x))


print(predictors)
with open(f"output/{__file__}.tex", "w") as outFile:
  for group in [predictors_main, predictors_other, predictors_cond]:
    for effect in group:
        print("&".join(flatten([[padEffect(effect.replace(".Resid", ""))]] + [sameLengthForEachEntry(models[x].get(effect, ["", "", ""])) for x in ["ff", "fp", "tt"]])) + " \\\\", file=outFile)
    print("\\hline", file=outFile)


