FF
# A tibble: 2 × 2
  Condition    ff
  <fct>     <dbl>
1 NoPreview  214.
2 Preview    194.

FP
# A tibble: 2 × 2
  Condition    fp
  <fct>     <dbl>
1 NoPreview  257.
2 Preview    213.

TT
# A tibble: 2 × 2
  Condition    tt
  <fct>     <dbl>
1 NoPreview  318.
2 Preview    262.

Fixations
# A tibble: 2 × 2
  Condition FIXATED
  <fct>       <dbl>
1 NoPreview   0.518
2 Preview     0.322
FF
# A tibble: 4 × 3
# Groups:   CorrectAnswerHasOccurred [2]
  CorrectAnswerHasOccurred Condition    ff
  <lgl>                    <fct>     <dbl>
1 FALSE                    NoPreview  215.
2 FALSE                    Preview    193.
3 TRUE                     NoPreview  214.
4 TRUE                     Preview    195.

FP
# A tibble: 4 × 3
# Groups:   CorrectAnswerHasOccurred [2]
  CorrectAnswerHasOccurred Condition    fp
  <lgl>                    <fct>     <dbl>
1 FALSE                    NoPreview  260.
2 FALSE                    Preview    213.
3 TRUE                     NoPreview  256.
4 TRUE                     Preview    213.

TT
# A tibble: 4 × 3
# Groups:   CorrectAnswerHasOccurred [2]
  CorrectAnswerHasOccurred Condition    tt
  <lgl>                    <fct>     <dbl>
1 FALSE                    NoPreview  330.
2 FALSE                    Preview    267.
3 TRUE                     NoPreview  310.
4 TRUE                     Preview    260.

Fixations
# A tibble: 4 × 3
# Groups:   CorrectAnswerHasOccurred [2]
  CorrectAnswerHasOccurred Condition FIXATED
  <lgl>                    <fct>       <dbl>
1 FALSE                    NoPreview   0.510
2 FALSE                    Preview     0.321
3 TRUE                     NoPreview   0.522
4 TRUE                     Preview     0.322
