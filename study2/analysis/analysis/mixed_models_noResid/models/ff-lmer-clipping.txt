
\begin{table}$
\begin{center}$
\begin{tabular}${l c}$
\hline
 & Model 1 \\
\hline
(Intercept)                                         & 199.76 & (4.36) &  $^{***}$ \\
PositionText                              & 0.52 & (0.41) &           \\
Condition                            & 16.61 & (8.68) &          \\
LogWordFreq                                & -3.63 & (0.63) &  $^{***}$  \\
IsNamedEntity                              & -5.50 & (1.53) &  $^{***}$  \\
IsCorrectAnswer                            & -3.31 & (3.43) &          \\
Surprisal                                  & 2.97 & (0.40) &  $^{***}$   \\
WordLength                      & 0.69 & (0.54) &           \\
LogWordFreq:WordLength & 1.31 & (0.44) &  $^{**}$    \\
LogWordFreq:IsNamedEntity         & -3.91 & (1.34) &  $^{**}$   \\
IsNamedEntity:Surprisal           & -1.22 & (0.88) &          \\
Condition:IsCorrectAnswer   & -13.68 & (5.13) &  $^{**}$  \\
\hline
AIC                                                 & 520965.17              \\
BIC                                                 & 521096.20              \\
Log Likelihood                                      & -260467.58             \\
Num. obs.                                           & 45968                  \\
Num. groups: tokenID                                & 4941                   \\
Num. groups: Participant                            & 22                     \\
Var: tokenID (Intercept)                            & 264.22                 \\
Var: Participant (Intercept)                        & 408.37                 \\
Var: Residual                                       & 4671.75                \\
\hline
\multicolumn{2}${l}${\scriptsize{ &  $^{***}$p<0.001;  &  $^{**}$p<0.01;  &  $^{*}$p<0.05}$}$
\end{tabular}$
\caption{Statistical models}$
\label{table:coefficients}$
\end{center}$
\end{table}$
