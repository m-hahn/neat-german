
\begin{table}$
\begin{center}$
\begin{tabular}${l c}$
\hline
 & Model 1 \\
\hline
(Intercept)                                         & 5.24 & (0.02) &  $^{***}$  \\
PositionText                              & 0.00 & (0.00) &          \\
Condition                            & 0.08 & (0.04) &          \\
LogWordFreq                                & -0.02 & (0.00) &  $^{***}$ \\
IsNamedEntity                              & -0.03 & (0.01) &  $^{***}$ \\
IsCorrectAnswer                            & -0.01 & (0.02) &         \\
Surprisal                                  & 0.01 & (0.00) &  $^{***}$  \\
WordLength                      & 0.00 & (0.00) &          \\
LogWordFreq:WordLength & 0.01 & (0.00) &  $^{***}$  \\
LogWordFreq:IsNamedEntity         & -0.02 & (0.01) &  $^{***}$ \\
IsNamedEntity:Surprisal           & -0.01 & (0.00) &         \\
Condition:IsCorrectAnswer   & -0.06 & (0.02) &  $^{*}$   \\
\hline
AIC                                                 & 31568.04              \\
BIC                                                 & 31699.18              \\
Log Likelihood                                      & -15769.02             \\
Num. obs.                                           & 46291                 \\
Num. groups: tokenID                                & 4945                  \\
Num. groups: Participant                            & 22                    \\
Var: tokenID (Intercept)                            & 0.01                  \\
Var: Participant (Intercept)                        & 0.01                  \\
Var: Residual                                       & 0.11                  \\
\hline
\multicolumn{2}${l}${\scriptsize{ &  $^{***}$p<0.001;  &  $^{**}$p<0.01;  &  $^{*}$p<0.05}$}$
\end{tabular}$
\caption{Statistical models}$
\label{table:coefficients}$
\end{center}$
\end{table}$
