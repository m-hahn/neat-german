import os
import random

files = map(lambda x:x.replace(".csv",""), os.listdir("/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/mappingWithExternal/"))
filesCNN = filter(lambda x: "cnn" in x, files)
filesDM = filter(lambda x: "daily" in x, files)

filesCNN = random.sample(filesCNN, 20)
filesDM = random.sample(filesDM, 20)

print 'return(c("'+'", "'.join(filesCNN+filesDM)+'"))'


