763
   http://web.archive.org/web/20150728170244id_/http://www.dailymail.co.uk/news/article-3053598/Mayweather-vs-Pacquaio-tickets-sell-watch-Periscope.html

--
Ringside tickets for the upcoming fight between Floyd Mayweather and Manny Pacquaio are fetching upwards of $130,000 online after official outlets sold out within minutes on Thursday night.
Due to the high Pay-per-view price of the fight ($89.99 - $99.99), its anticipated thousands of fight fans might try to watch the bout for free via a live video streaming app like Periscope or Meerkat.
If that were to happen, broadcasters Showtime and HBO could miss out on millions as viewers illegally stream the content between their devices without paying to watch the richest fight in history.
Launched by Twitter, Periscope allows users to broadcast a live stream to their social media followers. 
The video is then available to replay for up to 24 hours and can also be saved to the user's phone.
The app was recently used by many to watch the season five premiere of HBO's Game of Thrones.
'We are aware of Periscope and have sent takedown notices,' an HBO spokeswoman said in a statement.
'In general, we feel developers should have tools which proactively prevent mass copyright infringement from occurring on their apps and not be solely reliant upon notifications.' 
Around 500 tickets to the May 2 welterweight unification clash went on sale to the general public through Mgmgrand.com and Ticketmaster.com with prices beginning at $1,500 and ranging up to $7,500. 
The majority of seats in the 16,500-seater MGM Grand were already allocated to the hotel, Mayweather Promotions and Top Rank, the group that promotes Pacquiao. 
Five price categories were offered at face value but by Friday morning prices were varying from $5,900 to $130,000 on secondary sales sites like StubHub.
Prospective buyers were limited to four tickets per person. 
The MGM Grand's website experienced issues minutes before the 8pm start time due to high traffic.
The May 2 bout is one of the most anticipated in boxing history with the WBO, WBA and WBC titles all on the line.
There were fears the fight was under threat due to a stand-off over ticketing but the dispute came to an end on Wednesday evening when the contracts between the hotel and the promotional companies representing Mayweather and Pacquiao were signed.
Overall revenue from ticket sales is expected to scale $76million, breaking the previous record by three times and elevating the size and location of the rival camps' allocations at the MGM Grand's Garden Arena into a key area of negotiation.
Further money will be raised from closed-circuit broadcasts of the fight at MGM properties, with up to 50,000 tickets being made available at $150 apiece

--

Mayweather and Pacquiao will fight in Las Vegas, Nevada, on May 2 

Launched by Twitter, Periscope used to stream HBO's Game of Thrones 

Broadcasters Showtime and @placeholder could miss out on millions to streaming
+ HBO
