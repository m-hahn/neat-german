#!/usr/bin/perl -w

# note that for us in the ASC file 1 means "yes" and 2 means "no".

# output: a number of lines, each of which contains three fields:
# 1) item ID
# 2) correct answer (1=yes,0=no)
# 3) participant's response (1=yes,0=no)

while(<>) {
  chomp;
  chomp;
  if(/TRIALID ([^ ]+D[1-9])/) {
    $id = $1;
  }
  elsif(/QUESTION_ANSWER ([1234])/) {
    $correct = $1;
  }
  elsif(/^BUTTON\s+[0-9]+\s+([1234])\s+1/) {
    $response = $1;
    if($id && $correct) {
#      print "$id\t", $correct,"\t", $response, "\n"; # always comes last
      $total++;
      $all_correct++ if $response==$correct;
      $id = ""; $correct = "";
    }
  }
}

print STDERR "Correct: $all_correct/$total (" , ($all_correct/$total), "\%)\n";

# sub recode {
#   return ($_[0]==1 ? 1 : 0);
# }
